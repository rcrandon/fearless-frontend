window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            console.log("Bad Response!");
        } else {
            const data = await response.json();
            console.log(data)
            let selectTag = document.getElementById("location");

            for (let loc of data.locations) {
                let option = document.createElement("option");
                option.value = loc.id;
                option.innerHTML = loc.name;
                selectTag.appendChild(option);
            }
        }

        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
            }
        });

    } catch (e) {
        console.error("There's been an error!");
    }
})
